# Stack Web simple


## Procédure de déploiement en local

### Prérequis

* Installez docker desktop : [Docker Desktop](https://www.docker.com/products/docker-desktop/)

### Pour le projet Web simple :

* git clone du projet dans le répertoire de votre choix :  ```git clone [url]```

* Rendez vous dans le dossier Web simple une fois le projet cloné et ouvrez un terminal 

* Faites la commande: ```docker build -t nom_de_votre_image .```

* Rendez-vous sur docker desktop et allez dans la liste de vos images. Une fois votre image trouvé, faites run et choisissez un port (libre) pour la lancer. 

* Votre conteneur sera lancé et rendez-vous sur localhost:PORT_CHOISI





## Procédure de déploiement en PROD

### Prérequis

* Avoir un serveur de prod AWS revient à 350€ par mois avec une config : 
``` 
- Windows
- 16 go de ram
- 1 terra de stockage
- 4 processeurs
- SQL serveur standard
```

### Pour déployer sur AWS :

* Ouvrez laAWS DeepLens console à l'adresse [AWS] https://console.aws.amazon.com/deeplens/.

* Choisissez Projects (Projets), puis Create new project (Créer un projet).

* Sur l'écran Choose project type (Choisir un type de projet)

* Choisissez Use a project template (Utiliser un modèle de projet), puis l'exemple de projet que vous souhaitez créer. Pour cet exercice, choisissez Object detection (Détection d'objets).

* Faites défiler l'écran vers le bas, puis choisissez Next (Suivant).

* Sur l'écran Specify project details (Spécifier les détails du projet)

* Dans la section Project information (Informations sur le projet) :

* Acceptez le nom par défaut du projet ou entrez le nom de votre choix.

* Acceptez la description par défaut du projet ou entrez la description de votre choix.

* Choisissez Create (Créer).

* Cela vous renvoie à l'écran Projects (Projets) dans lequel figure le projet que vous venez de créer, aux côtés de vos autres projets.

* Sur l'écran Projects (Projets), choisissez la case d’option en regard de votre nom de projet ou choisissez le projet pour ouvrir la page des détails du projet, puis choisissez Deploy to device (Déployer sur l’appareil).

* Sur l'écran Target device (Appareil cible), dans la liste des appareils AWS DeepLens, choisissez la case d’option située à gauche de l'appareil sur lequel vous souhaitez déployer ce projet. Seul un projet à la fois peut être déployé sur un appareil AWS DeepLens.

* Choisissez Examiner.

* Si un projet est déjà déployé sur l'appareil, un message d'erreur vous signale que le déploiement de ce projet aura pour effet de remplacer le projet qui s'exécute déjà sur l'appareil. Choisissez Continue project (Continuer le projet).

* Vous accédez alors à l'écran Review and deploy (Examiner et déployer).

* Sur l'écran Review and deploy (Vérifier et déployer), passez en revue votre projet et choisissez Previous (Précédent) pour revenir en arrière et apporter des modifications ou Deploy (Déployer) pour déployer le projet.


